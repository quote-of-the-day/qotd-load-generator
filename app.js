const express = require('express');
const fs = require('fs');
const executor = require('./usecaseExecutor.js');
const moment = require('moment');
const utils = require('./utils.js');
const requestIp = require('request-ip');
// require('./proxy.js'); // proxy to override headers on request from browser


app = express();
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.set('port', 3011);

app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
app.use(requestIp.mw());

app.get('/histogram', function (req, res) {
    var histogram = executor.histogram();
    res.json( histogram );
});


app.get('/', function (req, res) {
    res.redirect('/version');
});

app.get('/version',
    function (req, res) {
        utils.log('/version');
        var ip = req.clientIp;
        res.send(`${appName} v${appVersion}, build: ${buildInfo}.  Your IP: ${ip}`);
    }
);

//----------------------------------------------------------------------------------------------------

const package = require('./package.json');
const appName = package.name;
const appVersion = package.version;
const buildInfo = fs.readFileSync('build.txt').toString().trim();

app.listen(app.get('port'), '0.0.0.0', function () {
    console.log(`Starting ${appName} v${appVersion}, Build: ${buildInfo} on port ${app.get('port')}`);
    // console.log(`Starting ${appName} v${appVersion}, ${buildInfo}`);
    _loadRunning = true;
    var now = new moment();
    var msg = `Load generator starting: ${now}`;
    utils.log( msg );
    executor.startLoad();
});



