const { createProxyMiddleware, responseInterceptor  } = require('http-proxy-middleware');
const express = require('express');

// proxy settings
const PORT = 4000;
const HOST = "localhost";

const server = express();

var user = null;

exports.setUser = function(newUser){
    user = newUser;
}



const proxy = createProxyMiddleware({
    target: process.env.QOTD_WEB_HOST,
    changeOrigin: true, // for vhosted sites
    // pathRewrite: {
    //     [`^/`]: '/',
    // },
    /**
     * IMPORTANT: avoid res.end being called automatically
     **/
    // selfHandleResponse: true, // res.end() will be called internally by responseInterceptor()

    onProxyReq(proxyReq, req, res) {
        // x-forwarded-for
        if( user ) {
            proxyReq.setHeader('x-qotd-user', user.userId )
            proxyReq.setHeader('X-Forwarded-For', user.ip );
            proxyReq.setHeader('referer', user.ip); 
        }
        // res.end();
    }
  });

server.use('/', proxy);

server.listen(PORT, HOST, () => {
    console.log(`Starting Proxy at ${HOST}:${PORT}`);
});