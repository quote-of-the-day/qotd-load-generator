const utils = require('./utils.js');
const moment = require('moment');
const { Builder, By } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const chromeDriver = require('selenium-webdriver/chrome');
const Spline = require('cubic-spline');
const async = require('async');

HEADLESS = true;
if (process.env.HEADLESS == "false") {
    HEADLESS = false;
}

// global so app can see
_loadRunning = false;

const minInDay = [    0,  240,   420,   600, 840, 960, 1140, 1320, 1440 ];
const delay    = [ 10000, 4000, 6000, 30000,   0, 100,    0,  100, 10000 ];
const Schedule = new Spline(minInDay,delay);

var numberOfSuitesRun = 0;
function getNumberOfSuitesRun() {
    return numberOfSuitesRun;
}

exports.getNumberOfSuitesRun=getNumberOfSuitesRun;

async function buildBrowser() {
    return new Promise( async (resolve,reject) => {
        if( _browser == null || typeof _browser == 'undefined' ) {
            if( process.env.USE_FIREFOX == "true" ){
                options = new firefox.Options();
                if (HEADLESS) {
                    options.headless();
                }
                try {
                    _browser = await new Builder()
                        .forBrowser('firefox')
                        .setFirefoxOptions(options)
                        .build();
                    resolve(_browser);
                } catch (error) {
                    utils.log(error);
                    reject(error);
                }     
            } else {
                const options = new chromeDriver.Options();
                options.addArguments(
                    '--no-sandbox',
                    // Use --disable-gpu to avoid an error from a missing Mesa library, as per
                    // https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md
                    '--disable-gpu',
                    '--disable-dev-shm-usage'
                );
                if( HEADLESS ) {
                    options.addArguments( 'headless' );
                }
                try {
                    _browser = await new Builder()
                        .forBrowser('chrome')
                        .setChromeOptions(options)
                        .build();
                    resolve(_browser);
                } catch (error) {
                    utils.log(error);
                    reject(error);
                }   
            }
   
    
        } else {
            resolve(_browser);
        }

            // const options = new firefox.Options();
        // const options = new chrome.Options();
    });
}

var starting = new moment();
function startingTime() {
    return starting;
}
exports.startingTime = startingTime;

var _browser = null;
// var url = "http://localhost:4000"; //process.env.QOTD_WEB_HOST;
var url = process.env.QOTD_WEB_HOST;
utils.log("URL: "+url,null,"INFO");


async function startLoad() {
    // delay start randomly, so that when multiple load generator instance are running 
    // they are not semi-synchronized and all start and end at approximately the same time.
    var startDelay = utils.intBetween(10, 5000); // up to 5 sec
    utils.log("Delayed start: " + startDelay + "ms" );

    setTimeout( async () => {
        numberOfSuitesRun = 0;
        runSuite();
    }, startDelay);

}
exports.startLoad = startLoad;

var useCasesFunctions = [ runDailyQuote, runRandomQuote, runAuthorDaily, runAuthorRandom, runPdfDaily, runPdfRandom, runOrderEngraving ];
if( process.env.QRCODE == 'true' ) {
    useCasesFunctions.push( runQrCodeDaily );
    useCasesFunctions.push( runQrCodeRandom );
}

const useCaseHistogram = {
    "Daily": 0,
    "Random": 0,
    "PDF Daily": 0,
    "PDF Random": 0,
    "Author Daily": 0,
    "Author Random": 0,
    "Order Engraving": 0,
    "QR Code Daily": 0,
    "QR Code Random": 0,
}

const keys = Object.keys(useCaseHistogram);

exports.histogram = function(){
    return useCaseHistogram;
}

async function runRandomUseCase(){
    return new Promise( async (resolve,reject) => {
        try{
            var index = utils.intBetween(0,useCasesFunctions.length);
            var key = keys[index];
            useCaseHistogram[key]++;
            var useCase = useCasesFunctions[index];
            await useCase();
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function incrementSuites(err){
    if( err ) {
        utils.log(err)
    }
    numberOfSuitesRun++;
    if( process.env.USE_SCHEDULE == 'false' ) { // default is true
        setTimeout(runSuite, 100);    
    } else {
        var now = new moment();
        var hour = now.hour();  // UTC
        var minIntoDay = hour * 60 + now.minutes();
        var delay = Schedule.at(minIntoDay); 
        setTimeout(runSuite, delay);    
    }
}

async function runSuite(){

    try{

        if( process.env.RANDOMIZE_SCENARIOS == 'true') {  // default is false

            async.timesSeries(9, runRandomUseCase, incrementSuites );

        } else {
            await runDailyQuote();
            await runRandomQuote();
            await runAuthorDaily();
            await runAuthorRandom();
            await runPdfDaily();
            await runPdfRandom();
            await runOrderEngraving();
            if( process.env.QRCODE == 'true' ) {
                await runQrCodeDaily();
                await runQrCodeRandom();
            }
            numberOfSuitesRun++;
            if( process.env.USE_SCHEDULE == 'false' ) {   // default is true
                setTimeout(runSuite, 100);    
            } else {
                var now = new moment();
                var hour = now.hour();  // UTC
                var minIntoDay = hour * 60 + now.minutes();
                var delay = Schedule.at(minIntoDay); 
                setTimeout(runSuite, delay);    
            }

        }

    } catch( error ) {
        // ok something nasty happened.  pause a bit then restart
        utils.log(error);
        utils.log("OK, clearly something nasty is happening.  It might just be the QotD services haven't started yet, or are restating.");
        utils.log("So we're going to take a break for a min, then give it another try.");
        setTimeout( startLoad, 60000 );
    }

}

function naptime(){
    return utils.genNormalInt(3000,2000,1000,6000)
}

// global user vars

// const USERS = JSON.parse(fs.readFileSync('./usersLoad.json'));
// var user = null;

// function changeUser() {
//     var userIndex = utils.intBetween(0,USERS.length);
//     user = USERS[userIndex];
//     proxy.setUser(user);
//     utils.log("new user: " + JSON.stringify(user), null, "INFO");
// }

function runDailyQuote(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "Daily";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runRandomQuote(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "Random";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnRandom')).click();
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runPdfDaily(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "PDF Daily";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnPDF')).click();
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runPdfRandom(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "PDF Random";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnRandom')).click();
            await browser.findElement(By.id('btnPDF')).click();
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runAuthorDaily(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "Author Daily";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            var link = await browser.findElement(By.id('author_link'));
            var href = link.getAttribute("href");
            await browser.navigate().to(href);
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runAuthorRandom(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "Author Random";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnRandom')).click();
            var link = await browser.findElement(By.id('author_link'));
            var href = link.getAttribute("href");
            await browser.navigate().to(href);
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runOrderEngraving(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "Order Engraving";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            try{
                await browser.findElement(By.id('btnOrder')).click();
                await browser.sleep(naptime());
                await browser.findElement(By.id('btnOrderEngraving')).click();
                await browser.sleep(naptime());
                await browser.findElement(By.id('btnCloseOrderInformationDialog')).click();
                await browser.sleep(naptime());    
            } catch( er ) {
                // expected error on systems not configured with engraving
                utils.log(er);
            }
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runQrCodeDaily(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "QR Code Daily";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnQrCode')).click();
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}

function runQrCodeRandom(){
    return new Promise( async (resolve,reject) => {
        try{
            currentUseCase = "QR Code Random";
            utils.log(currentUseCase);
            // changeUser();
            var browser = await buildBrowser();
            await browser.get(url);
            await browser.sleep(naptime());
            await browser.findElement(By.id('btnRandom')).click();
            await browser.findElement(By.id('btnQrCode')).click();
            await browser.sleep(naptime());
            resolve();
        } catch( error ) {
            reject(error);
        }
    });
}



