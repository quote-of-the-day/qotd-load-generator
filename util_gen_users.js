const faker = require('faker');
const fs = require('fs');

var usersLoad = [];
var usersWeb = {};

var addressbase = [
    "192.168.",
    "172.16.",
    "10.15.",
    "10.21.",
    "10.42.",
    "10.80.",
    "10.122.",
    "10.167.",
    "10.201.",
    "10.240."
]

// min inclusive , max exclusive 
function between(min, max) {  
    return Math.floor(
      Math.random() * (max - min) + min
    )
  }

for( var i=0; i<100; i++) {
    var fname = faker.name.firstName();
    var lname = faker.name.lastName();
    var name = fname + " " + lname;
    var email = faker.internet.exampleEmail(fname, lname);
    var userId = faker.internet.userName(fname, lname);

    var baseIndex = between(0, addressbase.length );
    var base = addressbase[baseIndex];
    var octet1 = between(0, 255 );
    var octet2 = between(0, 255 );

    var ip = base + octet1 + "." + octet2;

    var user = { "userId": userId, "userName": name, "email": email, "ip": ip }
    usersLoad.push(user);
    usersWeb[userId] = user;

}

fs.writeFileSync('usersLoad.json', JSON.stringify(usersLoad,null,4));
fs.writeFileSync('usersWeb.json', JSON.stringify(usersWeb,null,4));
